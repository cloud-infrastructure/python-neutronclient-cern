# CERN networking plugin

CERN networking client plugin out-of-tree. It extends [python-neutronclient](https://github.com/openstack/python-neutronclient) to support [networking_cern](https://gitlab.cern.ch/cloud-infrastructure/openstack-neutron-cern) plugin.

## Versioning

In order to facilitate the release process, the software major version is increased following OpenStack major releases:

| Version | OpenStack Release |
| ------- | ----------------- |
| 1.X.Y   | Train             |
| 2.X.Y   | Ussuri            |

## RPM distribution

The RPM distribution is implemented in a separate repository called [python-neutronclient-cern-distgit](https://gitlab.cern.ch/cloud-infrastructure/python-neutronclient-cern-distgit).

The main contract between the two repos is that this software must be released using the tag naming pattern: `X.Y.Z`.
