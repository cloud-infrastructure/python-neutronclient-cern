
#!/usr/bin/env python
# Copyright (c) 2013 Hewlett-Packard Development Company, L.P.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# THIS FILE IS MANAGED BY THE GLOBAL REQUIREMENTS REPO - DO NOT EDIT


PROJECT = 'networking_cern_client'

# Change docs/sphinx/conf.py too!
VERSION = '1.0.0'

from setuptools import setup, find_packages

try:
    long_description = open('README.rst', 'r').read()
except IOError:
    long_description = ''

setup(
    name=PROJECT,
    version=VERSION,

    description='Neutron CERN Networking Client plugin',
    long_description=long_description,

    author='CERN Cloud',
    author_email='cloud-developers@cern.ch',

    url='https://gitlab.cern.ch/cloud-infrastructure/python-neutronclient-cern',
    download_url='https://gitlab.cern.ch/cloud-infrastructure/python-neutronclient-cern/repository/archive.tar.gz?ref=master',

    classifiers=[
        'Environment :: OpenStack',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: Apache Software License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Programming Language :: Python :: 2,',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=[
        'osc-lib>=1.13.0',
        'python-neutronclient>=6.12.0'
        ],

    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,

    entry_points={
        'neutronclient.extension': [
            'cern_client_cluster = networking_cern_client._cluster',
            'cern_client_hostrestriction = networking_cern_client._hostrestriction'
        ],
        'openstack.cli.extension': [
            'networking_cern_client = networking_cern_client.osc.plugin'
        ],
        'openstack.networking_cern_client.v1': [
            'network_cluster_list = networking_cern_client.osc.v1.cluster:ListCERNCluster',
            'network_cluster_show = networking_cern_client.osc.v1.cluster:ShowCERNCluster',
            'network_cluster_create = networking_cern_client.osc.v1.cluster:CreateCERNCluster',
            'network_cluster_add_subnet = networking_cern_client.osc.v1.cluster:AddSubnetCERNCluster',
            'network_cluster_delete = networking_cern_client.osc.v1.cluster:DeleteCERNCluster',
            'network_cluster_remove_subnet = networking_cern_client.osc.v1.cluster:RemoveSubnetCERNCluster',
            'network_cluster_update = networking_cern_client.osc.v1.cluster:UpdateCERNCluster',
        ],
    },

    test_suite='nose.collector',
    tests_require=['nose'],

    zip_safe=False,
)
