#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#


from neutronclient.common import extension


class CERNHostRestriction(extension.NeutronClientExtension):
    resource = 'host'
    resource_plural = 'hosts'
    path = 'hosts'
    object_path = '/%s' % path
    resource_path = '/%s/%%s' % path
    versions = ['2.0']
    host_path = "/hosts/%s"


class ShowHost(extension.ClientExtensionShow, CERNHostRestriction):
    """Show restrictions about a given host."""

    shell_command = 'host'
    allow_names = False
