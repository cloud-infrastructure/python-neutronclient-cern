#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

import logging
import os

from osc_lib.cli import format_columns
from osc_lib.cli import parseractions
from osc_lib.command import command
from osc_lib import exceptions
from osc_lib import utils

CLUSTER = 'cluster'
CLUSTERS = 'clusters'

LOG = logging.getLogger(__name__)


class CreateCERNCluster(command.ShowOne):
    """create a cluster with a given name"""

    def get_parser(self, prog_name):
        parser = super(CreateCERNCluster, self).get_parser(prog_name)

        parser.add_argument(
            'NAME',
            metavar='<name>',
            help='Name of the CERN cluster to create')
        return parser

    def take_action(self, parsed_args):
        client = self.app.client_manager.neutronclient
        body = {
            CLUSTER: {
                'name': parsed_args.NAME,
            }
        }
        column_headers = (
            'id',
            'name',
            'subnets',
        )
        new_cluster = client.post('/clusters', body)
        data = new_cluster['cluster']
        return (column_headers, (data['id'], data['name'], data['subnets']))


class ListCERNCluster(command.Lister):
    """list of clusters"""

    def get_parser(self, prog_name):
        parser = super(ListCERNCluster, self).get_parser(prog_name)
        return parser

    def take_action(self, parsed_args):
        client = self.app.client_manager.neutronclient
        column_headers = (
            'id',
            'name',
            'subnet',
            'cidr',
        )
        data = client.get('/clusters')
        data_subnets = client.get('/subnets')
        final = []
        for subnets in data['clusters']:
            if not subnets['subnets']:
                final.append((subnets['id'], subnets['name'], '', ''))
            else:
                for i, subnet in enumerate(subnets['subnets']):
                    subnet_cidr = [x for x in data_subnets['subnets'] if x['id'] == subnet]
                    if i == 0:
                        final.append((subnets['id'], subnets['name'], subnet_cidr[0]['id'], subnet_cidr[0]['cidr']))
                    else:
                        final.append(('', '', subnet_cidr[0]['id'], subnet_cidr[0]['cidr']))
        return (column_headers, final)


class ShowCERNCluster(command.ShowOne):
    """show a cluster with a given Name or ID"""

    def get_parser(self, prog_name):
        parser = super(ShowCERNCluster, self).get_parser(prog_name)
        parser.add_argument(
            'id',
            metavar='<name_or_id>',
            help='Name or ID of the CERN cluster to show')
        return parser

    def take_action(self, parsed_args):
        client = self.app.client_manager.neutronclient
        column_headers = (
            'id',
            'name',
            'subnets',
        )
        if parsed_args.id.startswith('VMPOOL'):
            data = client.get('/clusters')
            wow_result = next(x for x in data['clusters'] if x['name'] == parsed_args.id)
            result = [
               wow_result['id'],
               wow_result['name'],
               '\n'.join(wow_result['subnets']),
            ]
        else:
            temp = client.show_cluster(parsed_args.id)['cluster']
            result = [
               temp['id'],
               temp['name'],
               '\n'.join(temp['subnets']),
            ]
        return (column_headers, result)


class AddSubnetCERNCluster(command.Command):
    """add subnet to a cluster"""

    def get_parser(self, prog_name):
        parser = super(AddSubnetCERNCluster, self).get_parser(prog_name)
        parser.add_argument(
            'CLUSTERID',
            metavar='<clusterid>',
            help='uuid of the cluster')
        parser.add_argument(
            'SUBNETID',
            metavar='<subnetid>',
            help='subnet to add to the cluster')
        return parser

    def take_action(self, parsed_args):
        path = 'clusters'
        object_path = '/%s' % path
        insert_path = 'insert_subnet'
        cluster_insert_subnet_path = '%s/%%s/%s' % (object_path, insert_path)

        client = self.app.client_manager.neutronclient
        body = {
            CLUSTER: {
                'subnet_id': parsed_args.SUBNETID
            }
        }
        client.put(cluster_insert_subnet_path % (parsed_args.CLUSTERID), body=body)


class DeleteCERNCluster(command.Command):
    """delete cluster"""

    def get_parser(self, prog_name):
        parser = super(DeleteCERNCluster, self).get_parser(prog_name)
        parser.add_argument(
            'CLUSTERID',
            metavar='<clusterid>',
            help='ID of the CERN cluster to delete')
        return parser

    def take_action(self, parsed_args):
        path = 'clusters'
        delete_cluster_path = '/%s/%%s' % (path)

        client = self.app.client_manager.neutronclient
        client.delete(delete_cluster_path % (parsed_args.CLUSTERID))


class RemoveSubnetCERNCluster(command.Command):
    """remove subnet from a cluster"""

    def get_parser(self, prog_name):
        parser = super(RemoveSubnetCERNCluster, self).get_parser(prog_name)
        parser.add_argument(
            'CLUSTERID',
            metavar='<clusterid>',
            help='uuid of the cluster')
        parser.add_argument(
            'SUBNETID',
            metavar='<subnetid>',
            help='subnet to remove from the cluster')
        return parser

    def take_action(self, parsed_args):
        path = 'clusters'
        object_path = '/%s' % path
        insert_path = 'remove_subnet'
        remove_subnet_cluster_path = '%s/%%s/%s' % (object_path, insert_path)

        client = self.app.client_manager.neutronclient
        body = {
            CLUSTER: {
                'subnet_id': parsed_args.SUBNETID
            }
        }
        client.put(remove_subnet_cluster_path % (parsed_args.CLUSTERID), body=body)


class UpdateCERNCluster(command.Command):
    """update cluster's information"""

    def get_parser(self, prog_name):
        parser = super(UpdateCERNCluster, self).get_parser(prog_name)
        parser.add_argument(
            'CLUSTERID',
            metavar='<clusterid>',
            help='uuid of the cluster to update')
        parser.add_argument(
            'NAME',
            metavar='<name>',
            help='new name of the cluster')
        return parser

    def take_action(self, parsed_args):
        path = 'clusters'
        update_cluster_path = '/%s/%%s' % (path)

        client = self.app.client_manager.neutronclient
        body = {
            CLUSTER: {
                'name': parsed_args.NAME
            }
        }
        client.put(update_cluster_path % (parsed_args.CLUSTERID), body=body)
