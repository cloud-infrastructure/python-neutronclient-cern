from osc_lib import utils


DEFAULT_API_VERSION = '1'

# Required by the OSC plugin interface
API_NAME = 'networking_cern_client'
API_VERSIONS = {
    '1': 'neutronclient.v2_0.client.Client',
}
API_VERSION_OPTION = 'os_networking_cern_client_api_version'

def make_client(instance):
    """Returns a neutron client."""
    neutron_client = utils.get_client_class(
        API_NAME,
        instance._api_version[API_NAME],
        API_VERSIONS)

    client = neutron_client()
    return client

def build_option_parser(parser):
    """Hook to add global options."""

    parser.add_argument(
        '--os-networking-cern-client-api-version',
        metavar='<networking-cern-client-api-version>',
       default=utils.env(
           'OS_NETWORKING_CERN_CLIENT_API_VERSION',
            default=DEFAULT_API_VERSION),
        help='CERN Networking Cern Client Plugin API version, default=' +
             DEFAULT_API_VERSION +
             ' (Env: OS_NETWORKING_CERN_CLIENT_API_VERSION)')
    return parser
