# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

Types of changes are: `added`, `changed`, `deprecated`, `removed`, `fixed` or `security`.

## [Unreleased]

## [1.0.0] - 2022-07-13

### Changed

- Aligned dependencies with OpenStack Train release

## [0.5.2] - 2019-01-23

### Fixed

- Fix for missing name_or_id argument in show cluster command

## [0.5.1] - 2018-07-27

### Fixed

- Fix for OpenStack network cluster list not showing networks without subnets

## [0.5.0] - 2018-07-19

### Added

- Add support for openstack extension delete_cluster
- Add support for openstack extension remove_subnet_cluster
- Add support for openstack extension update_cluster

## [0.4.0] - 2018-07-17

### Added

- Add support for openstack extension adding subnet to cluster

## [0.3.3] - 2018-05-11

### Fixed

- Fix openstack network cluster create

## [0.3.2] - 2018-03-06

### Fixed

- Fix openstack network cluster list

## [0.3.1] - 2018-03-06

### Fixed

- Fix openstack network cluster show, including ID and Name

## [0.3.0] - 2017-12-19

### Added

- Support openstack cli functionality

## [0.2.0] - 2017-10-06

### Added

- Add support for neutron cern extension in the openstack cli.

## [0.1.0] - 2017-09-28

### Removed

- Remove i18n import to adapt it to Pike version

## [0.0.1] - 2016-02-19

### Added

- Networking-cern client out-of-tree plugin first release
